package testcases;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;

public class HelloWorld1 
{
	public static void main(String[] args) throws MalformedURLException, InterruptedException
	{
		DesiredCapabilities cap = new DesiredCapabilities();
		cap.setCapability(MobileCapabilityType.AUTOMATION_NAME, "Appium");
		cap.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
		cap.setCapability(MobileCapabilityType.DEVICE_NAME, "emulator-5554");
		cap.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "io.selendroid.testapp");
		cap.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, "io.selendroid.testapp.HomeScreenActivity");
		
		AndroidDriver<MobileElement> driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), cap);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		String namein = "Mr.John", usernamein = "John", passin = "tajne", emailin = "john@denver.pl";
		 
		 //register button
		 MobileElement el1 = (MobileElement) driver.findElementByAccessibilityId("startUserRegistrationCD");
		 el1.click();
		 //io.selendroid.testapp:id/inputUsername
		 MobileElement el2 = (MobileElement) driver.findElementById("io.selendroid.testapp:id/inputUsername");
		 el2.sendKeys(usernamein);
		 //email of the customer
		 MobileElement el3 = (MobileElement) driver.findElementByAccessibilityId("email of the customer");
		 el3.sendKeys(emailin);
		 //io.selendroid.testapp:id/inputPassword
		 MobileElement el4 = (MobileElement) driver.findElementById("io.selendroid.testapp:id/inputPassword");
		 el4.sendKeys(passin);
    	 //hide keyboard
		 driver.pressKey(new KeyEvent(AndroidKey.BACK));
		 //io.selendroid.testapp:id/inputName
		 MobileElement el5 = (MobileElement) driver.findElementById("io.selendroid.testapp:id/inputName");
		 el5.clear();
		 el5.sendKeys(namein);
		 //io.selendroid.testapp:id/input_preferedProgrammingLanguage
		 MobileElement el6 = (MobileElement) driver.findElementById("io.selendroid.testapp:id/input_preferedProgrammingLanguage");
		 el6.click();
		 //program language		 
		 MobileElement el7 = (MobileElement) driver.findElementByXPath("//android.widget.CheckedTextView[6]");
		 el7.click();
		 //io.selendroid.testapp:id/input_adds
		 MobileElement el8 = (MobileElement) driver.findElementById("io.selendroid.testapp:id/input_adds");
		 el8.click();
		 //io.selendroid.testapp:id/btnRegisterUser
		 MobileElement el9 = (MobileElement) driver.findElementById("io.selendroid.testapp:id/btnRegisterUser");
		 el9.click();
		  //io.selendroid.testapp:id/label_name_data
		 MobileElement ass1 = (MobileElement) driver.findElementById("io.selendroid.testapp:id/label_name_data");
		 String nameout = ass1.getText();
		 //io.selendroid.testapp:id/label_username_data
		 MobileElement ass2 = (MobileElement) driver.findElementById("io.selendroid.testapp:id/label_username_data");
		 String usernameout = ass2.getText();
		 //io.selendroid.testapp:id/label_password_data
		 MobileElement ass3 = (MobileElement) driver.findElementById("io.selendroid.testapp:id/label_password_data");
		 String passout = ass3.getText();
		 //io.selendroid.testapp:id/label_email_data
		 MobileElement ass4 = (MobileElement) driver.findElementById("io.selendroid.testapp:id/label_email_data");
		 String emailout = ass4.getText();
		 //io.selendroid.testapp:id/label_preferedProgrammingLanguage_data
		 MobileElement ass5 = (MobileElement) driver.findElementById("io.selendroid.testapp:id/label_preferedProgrammingLanguage_data");
		 String prolanout = ass5.getText();
		 //io.selendroid.testapp:id/label_acceptAdds_data
		 MobileElement ass6 = (MobileElement) driver.findElementById("io.selendroid.testapp:id/label_acceptAdds_data");
		 String accept = ass6.getText();
		 

		 System.out.print(nameout + " ");
		 System.out.print(usernameout + " ");
		 System.out.print(passout + " ");
		 System.out.print(emailout + " ");
		 System.out.print(prolanout + " ");
		 System.out.print(accept + " ");
		
		Thread.sleep(3000);
		driver.quit();
	}
}
