package testcases;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;

public class HelloWorld2 
{
	public static void main(String[] args) throws MalformedURLException, InterruptedException
	{
		DesiredCapabilities cap = new DesiredCapabilities();
		cap.setCapability(MobileCapabilityType.AUTOMATION_NAME, "Appium");
		cap.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
		cap.setCapability(MobileCapabilityType.DEVICE_NAME, "emulator-5554");
		cap.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "io.selendroid.testapp");
		cap.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, "io.selendroid.testapp.HomeScreenActivity");
		
		AndroidDriver<MobileElement> driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), cap);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		String text01, text02;
		//visibleButtonTestCD
		MobileElement el1 = (MobileElement) driver.findElementByAccessibilityId("visibleButtonTestCD");
		el1.click();
		//io.selendroid.testapp:id/visibleTextView
		MobileElement el2 = (MobileElement) driver.findElementById("io.selendroid.testapp:id/visibleTextView");
		text01 = el2.getText();
		//showPopupWindowButtonCD
		MobileElement el3 = (MobileElement) driver.findElementByAccessibilityId("showPopupWindowButtonCD");
		el3.click();
		//PopupWindowHandling popup_dismiss_button btnDismiss
		//MobileElement el4 = (MobileElement) driver.findElementById("io.selendroid.testapp:id/popup_dismiss_button");
		//MobileElement el4 = (MobileElement) driver.findElementByClassName("btnDismiss");
		//text02 = el4.getText();
		/*
		 * 	public void displayPopupWindow(View view) {
		    LayoutInflater layoutInflater =
		        (LayoutInflater) getBaseContext().getSystemService(LAYOUT_INFLATER_SERVICE);
		    View popupView = layoutInflater.inflate(io.selendroid.testapp.R.layout.popup_window, null);
		    final PopupWindow popupWindow =
		        new PopupWindow(popupView, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		
		    Button btnDismiss =
		        (Button) popupView.findViewById(io.selendroid.testapp.R.id.popup_dismiss_button);
		    btnDismiss.setOnClickListener(new Button.OnClickListener() {
		
		      @Override
		      public void onClick(View v) {
		        // TODO Auto-generated method stub
		        popupWindow.dismiss();
		      }
		    });
		 */
				
		System.out.println(text01);
		//System.out.println(text02);
		
		Thread.sleep(3000);
		driver.quit();
	}
}
