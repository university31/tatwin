package testcases;

import java.util.concurrent.*;
import java.net.*;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.*;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;

public class Test08 {
	public static void main(String[] args) throws MalformedURLException,  InterruptedException {
		DesiredCapabilities cap = new DesiredCapabilities();
		cap.setCapability(MobileCapabilityType.AUTOMATION_NAME, "Appium");
		cap.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
		cap.setCapability(MobileCapabilityType.DEVICE_NAME, "emulator-5554");
		cap.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "com.android.contacts");
		cap.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, "com.android.contacts.activities.PeopleActivity");
		
		AndroidDriver<MobileElement> driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), cap);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		//add contact
		driver.findElement(By.id("com.android.contacts:id/floating_action_button")).click();
		//select opt about save
		driver.findElement(By.id("com.android.contacts:id/left_button")).click();
		//send name
		driver.findElementByXPath("//*[@text = \"" + "Name" + "\" ]").sendKeys("Chuck");
		//send name
		driver.findElementByXPath("//*[@text = \"" + "Phone" + "\" ]").sendKeys("123456789");
		//confirm
		driver.findElement(By.id("com.android.contacts:id/menu_save")).click();
		
		Thread.sleep(3000);
		driver.quit();
		
		}
}