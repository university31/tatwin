package testcases;

import java.util.concurrent.*;
import java.net.*;

import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.*;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;

public class Test07 {
	public static void main(String[] args) throws MalformedURLException,  InterruptedException {
		DesiredCapabilities cap = new DesiredCapabilities();
		cap.setCapability(MobileCapabilityType.AUTOMATION_NAME, "Appium");
		cap.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
		cap.setCapability(MobileCapabilityType.DEVICE_NAME, "emulator-5554");
		cap.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "com.google.android.apps.messaging");
		cap.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, "com.google.android.apps.messaging.ui.ConversationListActivity");
		
		AndroidDriver<MobileElement> driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), cap);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		//add mess
		driver.findElement(By.id("com.google.android.apps.messaging:id/start_new_conversation_button")).click();
		//send telephone number
		driver.findElement(By.id("com.google.android.apps.messaging:id/recipient_text_view")).sendKeys("457812457");
		//enter
		driver.pressKey(new KeyEvent(AndroidKey.ENTER));
		//type mess
		driver.findElement(By.id("com.google.android.apps.messaging:id/compose_message_text")).sendKeys("Pozdrawiam z gór");
		//send
		driver.findElement(By.id("com.google.android.apps.messaging:id/send_message_button")).click();
		
		Thread.sleep(3000);
		driver.quit();
		
		}
}