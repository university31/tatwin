package testcases;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.MobileElement;
import io.appium.java_client.MultiTouchAction;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.touch.offset.PointOption;


public class Test02
{
	public static void main(String[] args) throws MalformedURLException, InterruptedException
	{
		DesiredCapabilities cap = new DesiredCapabilities();
		cap.setCapability(MobileCapabilityType.AUTOMATION_NAME, "Appium");
		cap.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
		cap.setCapability(MobileCapabilityType.DEVICE_NAME, "emulator-5554");
		cap.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "io.selendroid.testapp");
		cap.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, "io.selendroid.testapp.HomeScreenActivity");
		
		AndroidDriver<MobileElement> driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), cap);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		 //touch test
		 MobileElement el1 = (MobileElement) driver.findElementById("io.selendroid.testapp:id/touchTest");
		 el1.click();
		 //canvas
		 MobileElement el2 = (MobileElement) driver.findElementById("io.selendroid.testapp:id/canvas_button");
		 el2.click();
		 TouchAction action1 = new TouchAction(driver);
		 TouchAction action2 = new TouchAction(driver);
		 TouchAction action3 = new TouchAction(driver);
		 TouchAction action4 = new TouchAction(driver);
		 TouchAction action5 = new TouchAction(driver);
		 TouchAction action6 = new TouchAction(driver);
		 //body
		 action1.press(PointOption.point(400,500));
		 action1.moveTo(PointOption.point(400,800));
		 Thread.sleep(1000);
		 action1.release();
		 //right arm
		 action2.press(PointOption.point(400,500));
		 action2.moveTo(PointOption.point(500,600));
		 Thread.sleep(1000);
		 action2.release();
		 //left arm
		 action3.press(PointOption.point(400,500));
		 action3.moveTo(PointOption.point(300,600));
		 Thread.sleep(1000);
		 action3.release();
		 //right leg
		 action4.press(PointOption.point(400,800));
		 action4.moveTo(PointOption.point(500,1000));
		 Thread.sleep(1000);
		 action4.release();
		 //left leg
		 action5.press(PointOption.point(400,800));
		 action5.moveTo(PointOption.point(300,1000));
		 Thread.sleep(1000);
		 action5.release();
		 //head nie wiem jak uzyc metody drawCircle w klasie canvas, albo jak w inny sposob mozna to narysować :D
		 action6.press(PointOption.point(400,400));
		 action6.moveTo(PointOption.point(425,425));
		 action6.moveTo(PointOption.point(450,450));
		 action6.moveTo(PointOption.point(425,475));
		 action6.moveTo(PointOption.point(400,500));
		 action6.moveTo(PointOption.point(375,475));
		 action6.moveTo(PointOption.point(350,450));
		 action6.moveTo(PointOption.point(375,425));
		 Thread.sleep(1000);
		 action6.release();
		    
		 //draw all touchs
		 MultiTouchAction act = new MultiTouchAction(driver);
		 act.add(action1).add(action2).add(action3).add(action4).add(action5).add(action6).perform();
		  
		driver.quit();
	}
}
