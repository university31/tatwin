package testcases;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.touch.offset.PointOption;


public class Test01
{
	public static void main(String[] args) throws MalformedURLException, InterruptedException
	{
		DesiredCapabilities cap = new DesiredCapabilities();
		cap.setCapability(MobileCapabilityType.AUTOMATION_NAME, "Appium");
		cap.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
		cap.setCapability(MobileCapabilityType.DEVICE_NAME, "emulator-5554");
		cap.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "io.selendroid.testapp");
		cap.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, "io.selendroid.testapp.HomeScreenActivity");
		
		AndroidDriver<MobileElement> driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), cap);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		 //touch test
		 MobileElement el1 = (MobileElement) driver.findElementById("io.selendroid.testapp:id/touchTest");
		 el1.click();
		 //canvas
//		 MobileElement el2 = (MobileElement) driver.findElementById("io.selendroid.testapp:id/canvas_button");
//		 el2.click();
		 TouchAction action1 = new TouchAction(driver);
		 action1.longPress(PointOption.point(200, 400));
		 action1.press(PointOption.point(350,400));
		 action1.moveTo(PointOption.point(300,400));
		 action1.release();
		 action1.tap(PointOption.point(400, 500));
		 action1.perform();
		 Thread.sleep(5000);
		 
//		 TouchAction action2 = new TouchAction(driver);
//		 action1.tap(point(45,45));
//		 MultiTouchAction act = new MultiTouchAction(driver);
//		 act.add(action1).add(action2).perform();
		  
		driver.quit();
	}
}
