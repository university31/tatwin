package testcases;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;


public class Test04
{
	public static void main(String[] args) throws MalformedURLException, InterruptedException
	{
		DesiredCapabilities cap = new DesiredCapabilities();
		cap.setCapability(MobileCapabilityType.AUTOMATION_NAME, "Appium");
		cap.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
		cap.setCapability(MobileCapabilityType.DEVICE_NAME, "emulator-5554");
		cap.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "com.android.chrome");
		cap.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, "com.google.android.apps.chrome.Main");
		AndroidDriver<MobileElement> driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), cap);
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		//akceptuj warunki
		driver.findElement(By.id("com.android.chrome:id/terms_accept")).click();
		//nie loguj
		driver.findElement(By.id("com.android.chrome:id/negative_button")).click();
		//wcisnij na pole wyszukiwania
		driver.findElement(By.id("com.android.chrome:id/search_box_text")).click();
		//napisz allegro.pl w polu url bar
		driver.findElement(By.id("com.android.chrome:id/url_bar")).sendKeys("https://allegro.pl");
		//enter
		driver.pressKey(new KeyEvent(AndroidKey.ENTER));
		//przelaczenie na kontekts webview chroma
		driver.context("WEBVIEW_chrome");	
		//zamkniecie regulaminu
		driver.findElement(By.xpath("//button[@class='_13q9y _8hkto _nu6wg _1sql3 _qdoeh _l7nkx _nyhhx']//img[@class='_nem5f']")).click();
		//wysylasz 'xiaomi' w pole wyszukiwania
		driver.findElement(By.xpath("//form[@class='_1h7wt _d25db_1HtwQ']//input[@placeholder='czego szukasz?']")).sendKeys("xiaomi");
		//nacisnij szukaj
		driver.findElement(By.xpath("//button[@class='_d25db_1t06j _13q9y _8tsq7 _1q2ua']")).click();
		//pobierz liczbe wyszukiwan
		String text = driver.findElement(By.className("_11fdd_39FjG")).getText();
		System.out.print(text);
		
		Thread.sleep(5000);
		driver.quit();
	}
}
