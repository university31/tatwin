*** Settings ***
Resource    MyKeywords.robot
Library     BuiltIn
*** Test Cases ***
Opening application
    Run application
    Welcome on Gmail
Send message
    Send message    sebkal1997@gmail.com    Test    Witaj serdecznie
Refresh messages
    swipe   400     400     400     600     1000
    sleep   2
Empty the thrash
    click element   ${MenuButton}
    sleep   1
    click element   ${Thrash}
    sleep   1
    click element   ${EmptyThrash}
    sleep   1
    click element   ${Cancel}
    sleep   1
    click element   ${MenuButton}
    sleep   1
    click element   ${RecievedMess}
    sleep   2
Archive a message several times
    FOR    ${INDEX}    IN RANGE     1   3
        swipe   600    400     100     400     1000
        sleep   2
    END
Open settings
    click element   ${MenuButton}
    sleep   1
    scroll down   ${Settings}
    #miałem problem z scrollowaniem
    #WebDriverException: Message: Unknown mobile command "scroll".
    #Only shell,scrollBackTo,viewportScreenshot,deepLink,startLogsBroadcast,stopLogsBroadcast,acceptAlert,dismissAlert,
    #batteryInfo,deviceInfo,changePermissions,getPermissions,performEditorAction commands are supported.
    sleep   1
    click element   ${Settings}
    sleep   1
    go back
    sleep   1
Favorite the message
    click element   ${FirstMessage}
    sleep   1
    click element   ${More}
    #musiałem tutaj dodać żeby klikało na te kropki od wiecej poniewaz przy pierwszy uruchomieniu wyskakiwal element
    #związany z tym
    go back
    sleep   1
    click element   ${Star}
    sleep   1
    go back
    sleep   1
Delete the message
    click element   ${FirstMessage}
    sleep   1
    click element   ${Delete}
    sleep   1
    go back
    sleep   1
Archive the message
    click element   ${FirstMessage}
    sleep   1
    click element   ${Archive}
    sleep   1
    go back
    sleep   1
Unread the message
    click element   ${FirstMessage}
    sleep   1
    click element   ${Unread}
    sleep   1
    go back
    sleep   1