*** Settings ***
Library     AppiumLibrary
*** Variables ***
${AppiumURL}    http://127.0.0.1:4723/wd/hub
${PlatformName}     Android
${PlatformVersion}  10
${PhoneName}    ca6e57fecd7a
${AppPackage}   com.google.android.gm
${AppActivity}  com.google.android.gm.ConversationListActivityGmail
#welcome
${WelcomeButton}    id=com.google.android.gm:id/welcome_tour_got_it
${OpenGmail}    id=com.google.android.gm:id/action_done
#send message
${NewMessageButton}     id=com.google.android.gm:id/compose_button
${SendTo}   id=com.google.android.gm:id/to
${Subject}  id=com.google.android.gm:id/subject
${Message}  xpath=//android.widget.EditText[@text = 'Napisz e-maila']
${Send}     id=com.google.android.gm:id/send
#way to thrash
${MenuButton}   class=android.widget.ImageButton
${Thrash}   xpath=//*[@text = 'Kosz']
${EmptyThrash}  id=empty_trash_spam_action
${Cancel}   id=android:id/button2
${RecievedMess}     xpath=//*[@text = 'Odebrane']
#settings
${Settings}     xpath=//*[@index = '14']
#first message
${FirstMessage}  xpath=//android.view.ViewGroup[@index = 2]
#more
${More}     xpath=//android.widget.ImageView[@index = 2]
#addtofavorite
${Star}     id=com.google.android.gm:id/conversation_header_star
#delete
${Delete}   id=com.google.android.gm:id/delete
#archive
${Archive}  id=com.google.android.gm:id/archive
#notreaded
${Unread}   id=com.google.android.gm:id/inside_conversation_unread
*** Keywords ***
Provided precondition
    Setup system under test
Run application
    open Application    ${AppiumURL}  newCommandTimeout=3600    logLevel=Error    platformName=${PlatformName}    platformVersion=${PlatformVersion}    deviceName=${PhoneName}    appPackage=${AppPackage}    appActivity=${AppActivity}
    sleep   2
Welcome on Gmail
    click element   ${WelcomeButton}
    sleep   6
    click element   ${OpenGmail}
    sleep   4
Send message
    [Arguments]     ${to}   ${title}    ${mess}
    click element   ${NewMessageButton}
    sleep   2
    input text  ${SendTo}   ${to}
    sleep   2
    input text  ${Subject}  ${title}
    sleep   2
    input text  ${Message}  ${mess}
    sleep   2
    click element   ${Send}