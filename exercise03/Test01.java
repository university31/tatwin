package testcases;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Image;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.android.nativekey.KeyEvent;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;



public class Test01 {
	public static void main(String[] args) throws MalformedURLException,  InterruptedException {
		DesiredCapabilities cap = new DesiredCapabilities();
		cap.setCapability(MobileCapabilityType.AUTOMATION_NAME, "Appium");
		cap.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
		cap.setCapability(MobileCapabilityType.DEVICE_NAME, "emulator-5554");
		cap.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "com.google.android.apps.messaging");
		cap.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, "com.google.android.apps.messaging.ui.ConversationListActivity");
		
		AndroidDriver<MobileElement> driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), cap);
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		String home_url = "C:\\Users\\Naukowiec\\Desktop\\JAVA_WORKSPACE\\tatwin03\\src\\imgs\\";
		//patterns
		//first test patterns
		Pattern addmess = new Pattern(home_url + "add_mess.png");
		Pattern send_to = new Pattern(home_url + "send_to.png");
		Pattern more_info = new Pattern(home_url + "more_info.png");
		Pattern place_on_home = new Pattern(home_url + "place_on_home.png");
		//second test patterns
		Pattern to_chuck = new Pattern(home_url + "to_chuck.png");
		Pattern attach = new Pattern(home_url + "attach.png");
		Pattern allow_camera = new Pattern(home_url + "allow_camera.png");
		Pattern allow_camera1 = new Pattern(home_url + "allow_camera1.png");
		Pattern confirm = new Pattern(home_url + "confirm.png");
		Pattern send = new Pattern(home_url + "send.png");
		//third test patterns
		Pattern delete = new Pattern(home_url + "delete.png");
		Pattern delete1 = new Pattern(home_url + "delete1.png");
		Pattern more_info_purple = new Pattern(home_url + "more_info_purple.png");
		
		Screen s = new Screen();
		//test01
		try{
            s.click(addmess);
            Thread.sleep(1000);
            s.click(send_to);
            Thread.sleep(1000);
            s.click(more_info);
            Thread.sleep(1000);
            s.click(place_on_home);
            Thread.sleep(1000);
            File srcFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
    		ITesseract instance = new Tesseract();
    		String result = null;
    		try {
    			result = instance.doOCR(srcFile);
    			System.out.println(result);
    		}catch(Exception e) {
    			System.err.println(e.getMessage());
    		}
    		if(result.contains("added to Home screen"))
    			System.out.println("Test pomyślny");
    		else
    			System.out.println("Test niepomyślny");
	    }
	    catch(FindFailed e){
	            e.printStackTrace();
	    }
		//four times back button
		driver.pressKey(new KeyEvent(AndroidKey.BACK));
		driver.pressKey(new KeyEvent(AndroidKey.BACK));
		driver.pressKey(new KeyEvent(AndroidKey.BACK));
		driver.pressKey(new KeyEvent(AndroidKey.BACK));
		//test02
		try{
            Thread.sleep(1000);
            s.click(addmess);
            Thread.sleep(1000);
            s.click(to_chuck);
            Thread.sleep(1000);
            s.click(attach);
            Thread.sleep(1000);
            s.click(allow_camera);
            Thread.sleep(1000);
            s.click(allow_camera1);
            Thread.sleep(1000);
            s.click(confirm);
            Thread.sleep(1000);
            s.click(send);
            Thread.sleep(1000);
            File srcFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
    		ITesseract instance = new Tesseract();
    		String result = null;
    		try {
    			result = instance.doOCR(srcFile);
    			System.out.println(result);
    		}catch(Exception e) {
    			System.err.println(e.getMessage());
    		}
    		if(result.contains("MMS is not available on this network"))
    			System.out.println("Test niepomyślny");
    		else
    			System.out.println("Test pomyślny");
	    }
	    catch(FindFailed e){
	            e.printStackTrace();
	    }
		//two times back button
		driver.pressKey(new KeyEvent(AndroidKey.BACK));
		driver.pressKey(new KeyEvent(AndroidKey.BACK));
		//test03
		try{
			Thread.sleep(1000);
            s.click(to_chuck);
            Thread.sleep(1000);
            s.click(more_info_purple);
            Thread.sleep(1000);
            s.click(delete);
            Thread.sleep(1000);
            s.click(delete1);
            System.out.print("Test pomyślny");
	    }
	    catch(FindFailed e){
	            e.printStackTrace();
	    }
			
		
		
		Thread.sleep(3000);
		driver.quit();
		
		}
}