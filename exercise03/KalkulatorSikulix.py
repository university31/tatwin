click(Region(8,736,33,26))
wait(1)
type("Kalkulator" + Key.ENTER)
wait(1)
loc = Location(55, 347)
#potegowanie
def potegowanie():
    click(loc.offset(117, 82)) #5
    click(loc.offset(116, -33)) #sgr

    
#reset
def reseto():
    click(loc.offset(124, -86)) 

  
#dodawanie
def dodawanie():
    click(loc.offset(117, 82)) #5
    click(loc.offset(276, 134)) #+
    click(loc.offset(117, 82)) #5
    click(loc.offset(272, 188)) #=


#odejmowanie
def odejmowanie():    
    click(loc.offset(117, 82)) #5
    click(loc.offset(278, 83)) #-
    click(loc.offset(117, 82)) #5
    click(loc.offset(272, 188)) #=


#mnożenie
def mnozenie():
    click(loc.offset(117, 82)) #5
    click(loc.offset(275, 24)) #*
    click(loc.offset(117, 82)) #5
    click(loc.offset(272, 188)) #=


#dzielenie
def dzielenie():
    click(loc.offset(117, 82)) #5
    click(loc.offset(278, -33)) #/
    click(loc.offset(117, 82)) #5
    click(loc.offset(272, 188)) #=


items = ("nic", "dodawanie", "odejmowanie", "mnozenie", "dzielenie", "potegowanie")
selected = select("Wybierz jaka operacje chcesz wykonac", options = items)
if selected == items[0]:
    popup("Nie wybrales nic")
    exit(1)
elif selected == items[1]:
    dodawanie()
    wait(1)
elif selected == items[2]:
    odejmowanie()
    wait(1)
elif selected == items[3]:
    mnozenie()
    wait(1)
elif selected == items[4]:
    dzielenie()
    wait(1)
elif selected == items[5]:
    potegowanie()
    wait(1)
else:
    popup("Cos poszlo nie tak")
