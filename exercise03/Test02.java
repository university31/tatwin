package testcases;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

import org.sikuli.script.FindFailed;
import org.sikuli.script.Location;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

public class Test02 {

		public static void main(String[] args) throws InterruptedException, AWTException
		{
			Screen s = new Screen();
			String home_url = "C:\\Users\\Naukowiec\\Desktop\\JAVA_WORKSPACE\\tatwin03\\src\\imgs\\test_2\\";
			//patterns
			Pattern start = new Pattern(home_url + "win_start.png");
			Pattern adobe_xd = new Pattern(home_url + "adobe_xd.png");
			Pattern web_temp = new Pattern(home_url + "web_temple.png");
			Pattern drag = new Pattern(home_url + "drag.png");
			Pattern rect = new Pattern(home_url + "rect.png");
			Pattern circle = new Pattern(home_url + "circle.png");
			Pattern triangle = new Pattern(home_url + "triangle.png");
			Pattern line = new Pattern(home_url + "line.png");
			Pattern pen = new Pattern(home_url + "pen.png");
			Pattern text = new Pattern(home_url + "text.png");
			Pattern grid = new Pattern(home_url + "repeat_grid.png");
			Pattern extend = new Pattern(home_url + "grid_extend_bottom.png");
			Pattern zoom = new Pattern(home_url + "zoom.png");
			Pattern llol = new Pattern(home_url + "text_llol.png");
			Pattern exit = new Pattern(home_url + "exit.png");
			Pattern discard = new Pattern(home_url + "discard.png");
			//test01
			try {
				Thread.sleep(1000);
				s.click(start);
				Thread.sleep(1000);
				s.click(adobe_xd);
				Thread.sleep(1000);
				s.click(web_temp);
				Thread.sleep(2000);
			} catch (FindFailed e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//test02 - draw rectangle
			try {
				Thread.sleep(1000);
				s.click(rect);
				Location loc = new Location(200, 200);
				s.dragDrop(loc, loc.offset(50, 50));
			} catch (FindFailed e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//test03 - draw circle
			try {
				Thread.sleep(1000);
				s.click(circle);
				Location loc = new Location(300, 200);
				s.dragDrop(loc, loc.offset(50, 50));
			} catch (FindFailed e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//test04 - draw triangle
			try {
				Thread.sleep(1000);
				s.click(triangle);
				Location loc = new Location(400, 200);
				s.dragDrop(loc, loc.offset(50, 50));
			} catch (FindFailed e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//test05 - draw line
			try {
				Thread.sleep(1000);
				s.click(line);
				Location loc = new Location(500, 200);
				s.dragDrop(loc, loc.offset(50, 50));
			} catch (FindFailed e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//test06 - drawing something by pen
			try {
				Thread.sleep(1000);
				s.click(pen);
				Location loc = new Location(600, 200);
				s.click(loc);
				s.click(loc.offset(50, 0));
				s.click(loc.offset(0, 50));
				s.click(loc.offset(50, 50));
				s.click(loc);
			} catch (FindFailed e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//test07 - add text
			try {
				Thread.sleep(1000);
				s.click(text);
				Location loc = new Location(200, 400);
				s.click(loc);
				Robot rob = new Robot();
				rob.delay(100);
				rob.keyPress(KeyEvent.VK_L);
				rob.delay(100);
				rob.delay(200);
				rob.keyPress(KeyEvent.VK_L);
				rob.delay(100);
				rob.keyPress(KeyEvent.VK_O);
				rob.delay(100);
				rob.keyPress(KeyEvent.VK_L);
				rob.delay(100);
				rob.keyRelease(KeyEvent.VK_L);
				rob.delay(100);
				rob.keyRelease(KeyEvent.VK_L);
				rob.delay(100);
				rob.keyRelease(KeyEvent.VK_O);
				rob.delay(100);
				rob.keyRelease(KeyEvent.VK_L);
				rob.delay(100);
			} catch (FindFailed e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//test08 - grid view
			try {
				Thread.sleep(1000);
				s.click(drag);
				Location loc = new Location(150, 150);
				s.dragDrop(loc, loc.offset(500, 150));
				s.click(grid);
				s.dragDrop(extend, loc.offset(300, 150));
			} catch (FindFailed e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//test09
			try {
				Thread.sleep(1000);
				s.click(zoom);
				s.click(llol);
				Thread.sleep(200);
				s.click(llol);
				Thread.sleep(200);
			} catch (FindFailed e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//test10
			try {
				Thread.sleep(1000);
				s.click(exit);
				Thread.sleep(300);
				s.click(discard);
			} catch (FindFailed e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

}


