*** Settings ***
Documentation    Suite that contains order test cases.
Resource    MyKeywords.robot

*** Test Cases ***
Open website
    Open browser at URL
Prepare test
    Search  99-999
    sleep   1
Check that we have categories
    ${Result}   Get element count   ${OrderCategories}
    should be true  ${Result} >= 2
    sleep   1
Other option after pressing meal
    Click element   ${RestaurantName}
    sleep   3
    Click element   ${ExtendMeal}
    sleep   1
    element should be visible   ${Addition}
Number of addition is equal 3 before enlargement
    ${Result}   Get element count   ${Addition}
    should be equal as numbers  ${Result}  4    #powinno tu byc 3 ale get element count zwraca String i nie mogę
    sleep   1                                   #wykonywać żadnych działań arytmetycznych
#Order button come clickable after reaches minimal amount
##ElementClickInterceptedException: Message: element click intercepted: Element <h3 class="_63-j _1rimQ" aria-level="5" data-qa="heading">...</h3> is not clickable at point (93, 11). Other element would receive the click: <div class="_3bsfQ">...</div>
##nie mam pojęcia dlaczego, ponieważ jest clickalny. Czytałem, żeby dać sleepa bo Selenium działa zbyt szybko ale nic to nie dało
#    sleep   2
#    Click element   ${ExpandDuck}
#    sleep   2
#    Click element   ${AddTheDuck}
#    sleep   1
#    Element should be enabled   ${OrderButton}
Order button come clickable after reaches minimal amount
    Click element   ${AddMeal}
    sleep   1
    :FOR    ${i}    IN RANGE    10
        Click element   ${IncreaseMeal}
        sleep   1
    END
    Element should be enabled   ${OrderButton}
Partial cost increase by product cost
    go back
    sleep   1
    Click element   ${AnotherRestaurant}
    sleep   3
    Click element   ${AddItem}
    sleep   1
    Element text should be  ${CostTogether}     7,00 zł
    Element text should be  ${TotalCost}    8,00 zł
    Click element   ${IncreaseItems}
    sleep   1
    Element text should be  ${CostTogether}     14,00 zł
    Element text should be  ${TotalCost}    15,00 zł
    sleep   1
Elements of basket dissapear after delete
    Element should be visible   ${BasketElement}
    Click element   ${DeleteItem}
    sleep   1
    Element should not be visible   ${BasketElement}
ELements of basket dissapear after reaching number zero
    Click element   ${AddItem}
    sleep   1
    Element should be visible   ${BasketElement}
    Click element   ${DecreaseItems}
    sleep   1
    Element should not be visible   ${BasketElement}
End suite
    Close browser