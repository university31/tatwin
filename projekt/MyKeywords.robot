*** Settings ***
Library     SeleniumLibrary
Library     BuiltIn
Library     String
*** Variables ***
${Result2}  ${EMPTY}
${Result4}  ${EMPTY}
${URL}  https://www.pyszne.pl
${Browser}  Chrome
#Elements
${Menu}  xpath=//button[@class='menu button-myaccount userlogin']
${CookiesButton}    xpath=/html/body/div[4]/section/article/button
#Menu - Elements
${RegisterButton}   xpath=//a[@class='button_form button-cta-small']
${SignUpButton}     xpath=//a[@class='button_form button-cta-small-inveted']
#Registration - Elements
${LastnameInput}    id=iaccountsurname
${EmailInput}   id=iaccountuser
${PasswordInput}    id=iaccountpass
${RepeatPasswordInput}  id=iaccountpass2
${NewsletterCheckbox}   id=inewsletter
${RegisterSubmit}   xpath=//input[@id='registerbutton']
${FeedbackText}     class=notificationfeedback
#Login - Elements
${LoginInput}   id=iusername
${PassInput}    id=ipassword
${LoginButton}  xpath=//input[@class='button_form button-cta-small']
${VerificationCode}  xpath=//div[contains(text(),'Kod weryfikacyjny')]
${FeedbackLoginText}     id=notification
#Search - Elements
${SearchInput}  xpath=//input[@id='imysearchstring']
${SearchButton}  id=submit_deliveryarea
${SearchResults}    xpath=/html/body/div[3]/div[2]/div/div[1]/div/div/div[1]/div[1]
${SearchResult}     xpath=//button[@class='topbar__title js-header-location-update show-location']
${ClickCancel}  xpath=//h1[@class='header__title']
${FeedbackSearchText}   xpath=//div[@id='ideliveryareaerror']
#Order - Elements
${OrderCategories}  xpath=//div[@class='swiper-wrapper']/a
${RestaurantName}   xpath=/html/body/div[3]/div[2]/div/div[4]/div[1]/div[1]/div[2]
${ExtendMeal}   xpath=/html/body/div[1]/div/div/section/div[1]/section/section[3]/div[2]/div[3]/div/div/div[2]
${Addition}     xpath=/html/body/div[1]/div/div/section/div[1]/section/section[3]/div[2]/div[3]/div/div/div[2]/div/div/div[3]/div[2]/div/div/div
${AddTheDuck}   xpath=/html/body/div[1]/div/div/section/div[1]/section/section[2]/div[2]/div/div/div/div[2]/div/div/div[3]/button
${ExpandDuck}   xpath=/html/body/div[1]/div/div/section/div[1]/section/section[2]/div[2]/div/div/div/div[1]/div/div/div/div/div[1]/h3
${AddMeal}  xpath=/html/body/div[1]/div/div/section/div[1]/section/section[3]/div[2]/div[3]/div/div/div[2]/div/div/div[4]/button
${IncreaseMeal}  xpath=/html/body/div[1]/div/div/aside/div/div/div[2]/div[2]/div/div[1]/div/div/div[2]/div[3]/div/div/div/div/div[2]/span
${OrderButton}  xpath=/html/body/div[1]/div/div/section/div[1]/section/section[1]/div[2]/div/div/div/div[2]
${AnotherRestaurant}    xpath=/html/body/div[3]/div[2]/div/div[4]/div[1]/div[2]
${AddItem}  xpath=/html/body/main/div/div/div[1]/div[3]/div/div[2]/div[2]/div/div[1]/div[2]
${CostTogether}  xpath=/html/body/header/div[4]/div[2]/div/div[8]/div[1]/span[2]
${ShipmentCost}  xpath=/html/body/header/div[4]/div[2]/div/div[8]/div[2]/span[2]
${TotalCost}    xpath=/html/body/header/div[4]/div[2]/div/div[8]/div[3]/span[2]
${DeleteItem}   xpath=/html/body/header/div[4]/div[2]/div/div[4]/div/div[1]/button
${DecreaseItems}   xpath=/html/body/header/div[4]/div[2]/div/div[4]/div/div[1]/div/button[1]
${IncreaseItems}    xpath=/html/body/header/div[4]/div[2]/div/div[4]/div/div[1]/div/button[2]
${BasketElement}    xpath=/html/body/header/div[4]/div[2]/div/div[4]/div
*** Keywords ***
Open browser at URL
    Open Browser    ${URL}  ${Browser}
    Set Selenium Implicit Wait  5 s
    Click element   ${CookiesButton}
Register
    [Arguments]     ${Lastname}=${EMPTY}     ${Email}=${EMPTY}    ${Password}=${EMPTY}     ${PasswordRepeat}=${EMPTY}
    Input text  ${LastnameInput}    ${Lastname}
    Input text  ${EmailInput}   ${Email}
    Input password  ${PasswordInput}    ${Password}
    Input password  ${RepeatPasswordInput}  ${PasswordRepeat}
    Click element   ${RegisterSubmit}
Login
    [Arguments]     ${Login}=${EMPTY}   ${Password}=${EMPTY}
    Input text  ${LoginInput}   ${Login}
    Input password  ${PassInput}    ${Password}
    Click element   ${LoginButton}
Search
    [Arguments]     ${Search}=${EMPTY}
    Click element  ${SearchInput}
    Clear element text  ${SearchInput}
    Input text  ${SearchInput}  ${Search}
    Click element   ${ClickCancel}
    Click element   ${SearchButton}