*** Settings ***
Documentation    Suite that contains search test cases.
Resource    MyKeywords.robot

*** Test Cases ***
Open website
    Open browser at URL
1.Empty search field
    Search  ${SPACE}
    sleep   1
    Element text should be  ${FeedbackSearchText}   Wprowadzony adres lub kod pocztowy nie istnieje. Sprawdź dane i spróbuj ponownie.
    Reload page
2.Just a city name in search field
    Search  Koszalin
    sleep   1
    ${Result2}  Get text   ${FeedbackSearchText}
    Element text should be  ${FeedbackSearchText}   Wprowadzony adres lub kod pocztowy nie istnieje. Sprawdź dane i spróbuj ponownie.
    Reload page
3.Just a post code in search field
    Search  75-444
    sleep   1
    Element should be visible   ${SearchResults}
    Go back
    sleep   1
    Reload page
    sleep   1
4.Just a city and street in search field
    Search  Koszalin ul.Zwycięstwa
    sleep   1
    ${Result4}  Get text   ${FeedbackSearchText}
    Element text should be  ${FeedbackSearchText}   Wprowadzony adres lub kod pocztowy nie istnieje. Sprawdź dane i spróbuj ponownie.
    Reload page
    sleep   1
5.Comparing 2 and 4 test case
    should be equal as strings  ${Result4}  ${Result2}
6.City that doesn`t exists
    Search  Panmichalowo
    sleep   1
    Element text should be  ${FeedbackSearchText}   Wprowadzony adres lub kod pocztowy nie istnieje. Sprawdź dane i spróbuj ponownie.
    Reload page
    sleep   1
7.Random letters in search field
    ${RandomWord}   generate random string  10  [LETTERS]
    Search  ${RandomWord}
    sleep   1
    Element text should be  ${FeedbackSearchText}   Wprowadzony adres lub kod pocztowy nie istnieje. Sprawdź dane i spróbuj ponownie.
    Reload page
    sleep   1
8.Post code from another country than Poland
    Search  53567   #Deutschland, Asbach
    sleep   1
    Element text should be  ${FeedbackSearchText}   Wprowadzony adres lub kod pocztowy nie istnieje. Sprawdź dane i spróbuj ponownie.
    Reload page
    sleep   1
9.Insert a post code equals 99-999
    Search  99-999
    sleep   1
    Element text should be  ${SearchResult}   Test, 99-999
End suite
    Close browser