*** Settings ***
Documentation    Suite that contains login test cases.
Resource    MyKeywords.robot

*** Test Cases ***
Open website
    Open browser at URL
Go to login
    Click element   ${Menu}
    Click element   ${SignUpButton}
All fields are empty
    Login
    sleep   1
    Element text should be    ${FeedbackLoginText}  Proszę, podaj swoją nazwę użytkownika (adres mailowy)
Just login field is empty
    Login   ${EMPTY}  GoodP@SS123
    sleep   1
    Element text should be    ${FeedbackLoginText}  Proszę, podaj swoją nazwę użytkownika (adres mailowy)
Just password field is empty
    Login   email@mail.com  ${EMPTY}
    sleep   1
    Element text should be    ${FeedbackLoginText}  Twój login jest nieprawidłowy. Sprawdź swoją nazwę użytkownika (e-mail) i / lub hasło i spróbuj ponownie.
Wrong login and password
    Login   email@mail.com  GoodP@SS123
    sleep   1
    Element text should be    ${FeedbackLoginText}  Twój login jest nieprawidłowy. Sprawdź swoją nazwę użytkownika (e-mail) i / lub hasło i spróbuj ponownie.
Wrong email format
    Login   email.mail.com  GoodP@SS123
    sleep   1
    Element text should be    ${FeedbackLoginText}  Twój login jest nieprawidłowy. Sprawdź swoją nazwę użytkownika (e-mail) i / lub hasło i spróbuj ponownie.
No email in database
    Login   email@mail.com  GoodP@SS123
    sleep   1
    Element text should be    ${FeedbackLoginText}  Twój login jest nieprawidłowy. Sprawdź swoją nazwę użytkownika (e-mail) i / lub hasło i spróbuj ponownie.
Wrong password
    Login   email@mail.com  GoodP@SS123
    sleep   1
    Element text should be    ${FeedbackLoginText}  Twój login jest nieprawidłowy. Sprawdź swoją nazwę użytkownika (e-mail) i / lub hasło i spróbuj ponownie.
Correct login
    Login   sebkal1997@gmail.com  haslozostalozatajone
    sleep   1
    Element should be visible    ${VerificationCode}
End suite
    Close browser