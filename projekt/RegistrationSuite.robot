*** Settings ***
Documentation    Suite that contains registration test cases.
Resource    MyKeywords.robot
*** Test Cases ***
Open website
    Open browser at URL
Go to registration
    Click element   ${Menu}
    Click element   ${RegisterButton}
Empty fields register
    Register
    sleep   1
    Element text should be    ${FeedbackText}    Twoja nazwa użytkownika jest niepoprawna. Użyj ważnego adresu e-mail.
Just lastname field is empty
    Register    ${EMPTY}    email@mail.com  GoodP@SS123  GoodP@SS123
    sleep   1
    Element text should be    ${FeedbackText}    Proszę wypełnić przynajmniej imię lub nazwisko.
Just email field is empty
    Register    Testowski    ${EMPTY}  GoodP@SS123  GoodP@SS123
    sleep   1
    Element text should be    ${FeedbackText}    Twoja nazwa użytkownika jest niepoprawna. Użyj ważnego adresu e-mail.
Just password field is empty
    Register    Testowski    email@mail.com  ${EMPTY}  GoodP@SS123
    sleep   1
    Element text should be    ${FeedbackText}    Twoje hasło powinno składać się z przynajmniej 8 znaków, zawierać zarówno małą i wielką literę oraz cyfrę i symbol.
Just repeat password field is empty
    Register    Testowski    email@mail.com  GoodP@SS123    ${EMPTY}
    sleep   1
    Element text should be    ${FeedbackText}    Hasła nie pasują do siebie, sprawdź hasła i spróbuj ponownie.
Password incorrectly repeated
    Register    Testowski    email@mail.com  GoodP@SS123  GoodP@SS321
    sleep   1
    Element text should be    ${FeedbackText}    Hasła nie pasują do siebie, sprawdź hasła i spróbuj ponownie.
Password has less than 8 signs
    Register    Testowski    email@mail.com  P@ss123  P@ss123
    sleep   1
    Element text should be    ${FeedbackText}    Twoje hasło powinno składać się z przynajmniej 8 znaków, zawierać zarówno małą i wielką literę oraz cyfrę i symbol.
Password without special signs
    Register    Testowski    email@mail.com  GoodPASS123  GoodPASS123
    sleep   1
    Element text should be    ${FeedbackText}    Twoje hasło powinno składać się z przynajmniej 8 znaków, zawierać zarówno małą i wielką literę oraz cyfrę i symbol.
Password without big letters
    Register    Testowski    email@mail.com  goodp@ss123  goodp@ss123
    sleep   1
    Element text should be    ${FeedbackText}    Twoje hasło powinno składać się z przynajmniej 8 znaków, zawierać zarówno małą i wielką literę oraz cyfrę i symbol.
Password without numbers
    Register    Testowski    email@mail.com  GoodP@SS  GoodP@SS
    sleep   1
    Element text should be    ${FeedbackText}    Twoje hasło powinno składać się z przynajmniej 8 znaków, zawierać zarówno małą i wielką literę oraz cyfrę i symbol.
Wrong email format
    Register    Testowski    e-mail  GoodP@SS123  GoodP@SS123
    sleep   1
    Element text should be    ${FeedbackText}    Twoja nazwa użytkownika jest niepoprawna. Użyj ważnego adresu e-mail.
Email exists in database
    Register    Testowski    sebkal1997@gmail.com  GoodP@SS123  GoodP@SS123
    sleep   1
    Element text should be    ${FeedbackText}    Konto z tą nazwą użytkownika (adres e-mail) już istnieje. Kliknij tutaj aby odzyskać hasło. Jeśli wprowadzisz nieprawidłowy adres e-mail, sprawdź nazwę użytkownika poniżej i spróbuj ponownie.
End suite
    Close browser