package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class PageObject {
	WebDriver driver;
	
	@FindBy(xpath = "//input[@id='pwd']")
	WebElement passwordInput;
	
	@FindBy(xpath = "//body//input[3]")
	WebElement loginButton;

	@FindBy(xpath = "//input[@id='usr']")
	WebElement usernameInput;

	@FindBy(xpath = "//div[@id='case_login']//h3")
	WebElement resultText;
	
	@FindBy(xpath = "//div[@id='case_login']//a")
	WebElement backButton;
	
	//constructor
	public PageObject(WebDriver driver){
		this.driver = driver;
	    PageFactory.initElements(driver, this);
	}
	//methods
	public void enterUsername(String username){
		usernameInput.sendKeys(username);
	}
	 
	public void enterPassword(String password){
		passwordInput.sendKeys(password);
	}
	 
	public void login(){
		loginButton.click();
	}
	
	public String getResult(){
		return resultText.getText();
	}
	
	public void goBack(){
		backButton.click();
	}
	
}
