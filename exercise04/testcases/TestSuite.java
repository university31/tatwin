package testcases;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;

import pages.PageObject;

public class TestSuite {
	public ExtentHtmlReporter htmlReport;
	public ExtentReports extent;
	public ExtentTest testPositive;
	public ExtentTest testNegative;
	public ExtentTest testSkipped;
	public String driverPath;
	public WebDriver driver;
	public PageObject pageObject;
		@BeforeTest
		public void startTest()
		{
			//define driver
			driverPath = "C:\\driver\\chromedriver.exe";
			System.setProperty("webdriver.chrome.driver", driverPath);
			driver = new ChromeDriver();
	        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	        driver.get("http://testing-ground.scraping.pro/login");
	        pageObject = new PageObject(driver);
	        
			//setting report 
			htmlReport = new ExtentHtmlReporter("test-output//testPOM.html");
			htmlReport.config().setDocumentTitle("My official report");
			htmlReport.config().setReportName("Automatic reports generation");
			htmlReport.config().setTestViewChartLocation(ChartLocation.TOP);
			htmlReport.config().setTheme(Theme.STANDARD);
			
			extent = new ExtentReports();
			extent.attachReporter(htmlReport);
			extent.setSystemInfo("UserName", "Sebastian Kalinowski");
			
		}
		@Test
		public void firstTest()
		{
			testPositive = extent.createTest("Positive Test");
			
			String correctUsername = "admin";
		    String correctPassword = "12345";
		 
		    pageObject.enterUsername(correctUsername);
		    pageObject.enterPassword(correctPassword);
		    pageObject.login();
		 
		    Assert.assertTrue(pageObject.getResult().contains("WELCOME :)")); 
		}
		
		@Test
		public void secondTest()
		{
			testNegative = extent.createTest("Negative Test");
			
			String correctUsername = "admin";
		    String incorrectPassword = "admin";
		    
		    pageObject.goBack();
		    pageObject.enterUsername(correctUsername);
		    pageObject.enterPassword(incorrectPassword);
		    pageObject.login();
		 
		    Assert.assertTrue(pageObject.getResult().contains("WELCOME :)"));
		}
		@AfterMethod
		public void afterFail()
		{
			File srcFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			try {
				FileWriter writer = new FileWriter(srcFile);
				writer.write("File is writing");
				writer.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		@Test
		public void thirdTest()
		{
			testSkipped = extent.createTest("Skipped Test");			

			pageObject.goBack();
			if(pageObject.getResult().contains("Please, login:"))
			{
				throw new SkipException("Testing skip.");
			}
			else {
				Assert.assertTrue(false);
			}
		}
		@AfterTest
		public void endTest()
		{
			testPositive.log(Status.PASS, "Passed");
			testNegative.log(Status.FAIL, "Failed");
			testSkipped.log(Status.SKIP, "Skipped");
			extent.flush();
		}
		
}
