package testcases;

import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class Test01 {
	public ExtentHtmlReporter htmlReport;
	public ExtentReports extent;
	public ExtentTest testPositive;
	public ExtentTest testNegative;
	public ExtentTest testSkipped;
		@BeforeTest
		public void startTest()
		{
			htmlReport = new ExtentHtmlReporter("test-output//test.html");
			htmlReport.config().setDocumentTitle("My official report");
			htmlReport.config().setReportName("Automatic reports generation");
			htmlReport.config().setTestViewChartLocation(ChartLocation.TOP);
			htmlReport.config().setTheme(Theme.STANDARD);
			
			extent = new ExtentReports();
			extent.attachReporter(htmlReport);
			extent.setSystemInfo("UserName", "Sebastian Kalinowski");
		}
		@Test
		public void firstTest()
		{
			testPositive = extent.createTest("Positive Test");
			Assert.assertTrue(true);
		}
		@Test
		public void secondTest()
		{
			testNegative = extent.createTest("Negative Test");
			Assert.assertTrue(false);
		}
		@Test
		public void thirdTest()
		{
			testSkipped = extent.createTest("Skipped Test");
			throw new SkipException("Testing skip.");
		}
		@AfterMethod
		public void afterSkipping()
		{
		}
		@AfterTest
		public void endTest()
		{
			testPositive.log(Status.PASS, "Passed");
			testNegative.log(Status.FAIL, "Failed");
			testSkipped.log(Status.SKIP, "Skipped");
			extent.flush();
		}
		
}
